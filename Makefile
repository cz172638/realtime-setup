VERSION	:=	$(shell awk '/^Version:/ { print $$2 }' realtime-setup.spec)
$(info "Version: $(VERSION)")
RPMDIR	:=	$(shell pwd)/rpm
RPMARGS	:=	--define "_topdir $(RPMDIR)"

CC	:=	gcc
ifeq ($(CFLAGS),)
CFLAGS	:= 	-O3 -g -Wall -D_GNU_SOURCE \
		-fstack-protector-all -fcf-protection \
		-fstack-clash-protection
endif
CFLAGS  +=	-fPIC -fplugin=annobin
LDFLAGS +=	-Wl,-z,now -Wl,-z,relro -pie

DISTVER	:=	9.0.0-beta
DISTGITDIR :=	../realtime-setup.rhpkg
DISTGITBRANCH := rhel-$(DISTVER)

FILES	:=	slub_cpu_partial_off 	\
		rhel-rt.rules 		\
		kernel-is-rt 		\
		realtime-setup.sysconfig 	\
		realtime-setup.systemd 	\
		realtime.conf 		\
		realtime-entsk.service 	\
		realtime-setup.service	\
		realtime-setup.spec

all:  realtime-entsk

realtime-entsk: enable-netsocket-tstamp-static-key.c
	$(CC) $(CFLAGS) -c enable-netsocket-tstamp-static-key.c
	$(CC) $(LDFLAGS) -o realtime-entsk enable-netsocket-tstamp-static-key.o

rpm:	rpmdir tarball
	cp realtime-setup-$(VERSION).tar.bz2 rpm/SOURCES
	rpmbuild $(RPMARGS) -ba realtime-setup.spec

rpmdir:
	@[ -d rpm ] || mkdir -p rpm/{BUILD,BUILDROOT,RPMS,SOURCES,SPECS,SRPMS}

clean:
	rm -f *~ *.tar.bz2
	rm -rf rpm
	rm -f realtime-entsk *.o

tarball: rpmdir
	git archive --format=tar --prefix=realtime-setup-$(VERSION)/ HEAD | bzip2 >realtime-setup-$(VERSION).tar.bz2

install:
	install -m 755 -D slub_cpu_partial_off $(DEST)/usr/bin/slub_cpu_partial_off
	install -m 644 -D rhel-rt.rules $(DEST)/etc/udev/rules.d/99-rhel-rt.rules
	install -m 755 -D kernel-is-rt $(DEST)/usr/sbin/kernel-is-rt
	install -m 644 -D realtime-setup.sysconfig $(DEST)/etc/sysconfig/realtime-setup
	install -m 755 -D realtime-setup.systemd $(DEST)/usr/bin/realtime-setup
	install -m 644 -D realtime.conf $(DEST)/etc/security/limits.d/realtime.conf
	install -m 644 -D realtime-entsk.service $(DEST)/usr/lib/systemd/system/realtime-entsk.service
	install -m 755 -D realtime-entsk $(DEST)/usr/sbin/realtime-entsk
	install -m 644 -D realtime-setup.service $(DEST)/usr/lib/systemd/system/realtime-setup.service

dist-git: tarball
	cd $(DISTGITDIR) && rhpkg switch-branch --fetch $(DISTGITBRANCH)
	cp $(FILES) $(DISTGITDIR)
	cp realtime-setup-$(VERSION).tar.bz2 $(DISTGITDIR)
	cd $(DISTGITDIR) && rhpkg new-sources realtime-setup-$(VERSION).tar.bz2
	@echo "files copied to $(DISTGITDIR)"

scratch: rpm
	brew scratch $(DISTGITBRANCH) rpm/SRPMS/realtime-setup-$(VERSION).*.src.rpm
